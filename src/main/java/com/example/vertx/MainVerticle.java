package com.example.vertx;

import com.example.vertx.roles.RoleApi;
import com.example.vertx.roles.RoleDao;
import com.example.vertx.roles.RoleService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        Router router = Router.router(vertx);

        // Roles API
        RoleDao roleDao = new RoleDao();
        RoleApi roleApi = new RoleApi(roleDao, new RoleService());
        roleDao.createRoles();

        router.route("/api/roles*").handler(BodyHandler.create());
        router.get("/api/roles").handler(roleApi::getAll);
        router.post("/api/roles").handler(roleApi::addOne);
        router.delete("/api/roles/:id").handler(roleApi::deleteOne);
        router.get("/api/roles/:id").handler(roleApi::getOne);
        router.put("/api/roles/:id").handler(roleApi::updateOne);

        // Serve static resources
        router.route("/*").handler(StaticHandler.create());

        // Start server
        vertx.createHttpServer().requestHandler(router::accept).listen(8080, http -> {
            if (http.succeeded()) {
                startFuture.complete();
                System.out.println("HTTP server started on http://localhost:8080");
            } else {
                startFuture.fail(http.cause());
            }
        });

    }

}
