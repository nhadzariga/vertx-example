package com.example.vertx.roles;

import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;

import java.util.HashMap;
import java.util.Map;

public class RoleApi {

    private RoleDao roleDao;
    private RoleService roleService;

    public RoleApi(RoleDao roleDao, RoleService roleService) {
        this.roleDao = roleDao;
        this.roleService = roleService;
    }

    private Map<Integer, Role> roles = new HashMap<>();

    public void getAll(RoutingContext req) {
        req.response()
            .putHeader("content-type", "application/json; charset=utf-8")
            .end(Json.encodePrettily(roleDao.readAll()));
    }

    public void addOne(RoutingContext req) {

        final Role role = Json.decodeValue(req.getBodyAsString(), Role.class);

        if (isRoleUnique(role)) {
            roles.put(role.getId(), role);
            req.response()
                .setStatusCode(201)
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(role));
        } else {
            req.response()
                .setStatusCode(409)
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(new Error("Duplicate role found for this process already.")));
        }

    }

    private boolean isRoleUnique(Role role) {
        for (Role r : roles.values()) {
            if (r.getRole().equals(role.getRole())) {
                return false;
            }
        }
        return true;
    }

    public void deleteOne(RoutingContext req) {
        String id = req.request().getParam("id");
        if (id == null) {
            req.response().setStatusCode(400).end();
        } else {
            Integer idAsInteger = Integer.valueOf(id);
            roles.remove(idAsInteger);
        }
        req.response().setStatusCode(204).end();
    }

    public void getOne(RoutingContext req) {
        String id = req.request().getParam("id");
        if (id == null) {
            req.response().setStatusCode(400).end();
        } else {
            Integer idAsInteger = Integer.valueOf(id);
            Role role = roles.get(idAsInteger);
            if (roles == null) {
                req.response().setStatusCode(404).end();
            } else {
                req.response().setStatusCode(200)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(role));
            }
        }
    }

    public void updateOne(RoutingContext req) {
        String id = req.request().getParam("id");
        final Role inputRole = Json.decodeValue(req.getBodyAsString(), Role.class);
        if (id == null) {
            req.response().setStatusCode(400).end();
        } else {
            Integer idAsInteger = Integer.valueOf(id);
            Role role = roles.get(idAsInteger);
            if (roles == null) {
                req.response().setStatusCode(404).end();
            } else {
                role.setRole(inputRole.getRole());
                role.setUserId(inputRole.getUserId());
                req.response().setStatusCode(200)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(role));
            }
        }
    }

}
