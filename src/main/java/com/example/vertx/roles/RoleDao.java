package com.example.vertx.roles;

import java.util.HashMap;
import java.util.Map;

public class RoleDao {

    private Map<Integer, Role> roles = new HashMap<>();

    public void createRoles() {
        Role role1 = new Role("Team Leader");
        Role role2 = new Role("Operations Engineer");
        roles.put(role1.getId(), role1);
        roles.put(role2.getId(), role2);
    }

    public Map<Integer, Role> readAll() {
        return roles;
    }

    public Role readById(int id) {
        // TODO
        return null;
    }

    public void create(Role role) {
        // TODO
    }

    public void update(int id, Role role) {
        // TODO
    }

    public void delete(int id) {
        // TODO
    }

}
