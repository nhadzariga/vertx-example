package com.example.vertx.roles;

import java.util.concurrent.atomic.AtomicInteger;

public class Role {

    private static final AtomicInteger COUNTER = new AtomicInteger();

    private final int id;

    private String role;

    private Integer userId;

    public Role() {
        this.id = COUNTER.getAndIncrement();
    }

    public Role(String role) {
        this.id = COUNTER.getAndIncrement();
        this.role = role;
    }

    public int getId() {
        return this.id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
