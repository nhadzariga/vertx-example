package com.example.vertx;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(VertxExtension.class)
public class TestMainVerticle {

    @BeforeEach
    void deploy_verticle(Vertx vertx, VertxTestContext testContext) {
        vertx.deployVerticle(new MainVerticle(), testContext.succeeding(id -> testContext.completeNow()));
    }

    @Test
    @DisplayName("Should start a Web Server on port 8080")
    @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
    void start_http_server(Vertx vertx, VertxTestContext testContext) throws Throwable {
        vertx.createHttpClient().getNow(8080, "localhost", "/", response -> testContext.verify(() -> {
            assertTrue(response.statusCode() == 200);
            response.handler(body -> {
                assertTrue(body.toString().contains("My Example Process"));
                testContext.completeNow();
            });
        }));
    }

    @Test
    @DisplayName("GET /api/roles should return a list of roles")
    @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
    void testRoleApi(Vertx vertx, VertxTestContext testContext) {
        vertx.createHttpClient().getNow(8080, "localhost", "/api/roles", response -> testContext.verify(() -> {
            assertTrue(response.statusCode() == 200);
            response.handler(body -> {
                assertTrue(body.toJsonObject().size() == 2);
                assertTrue(body.toJsonObject().containsKey("0"));
                assertTrue(body.toJsonObject().containsKey("1"));
                testContext.completeNow();
            });
        }));
    }

}
